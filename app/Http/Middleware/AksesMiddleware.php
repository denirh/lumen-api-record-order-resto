<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request; 

class AksesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // if(auth()->check() && !auth()->user()->punyaRole($roleName)){
        //     return view('auth::pages.403');                                
        // }
        if(\Auth::check() && !User::hasRole($role)){
            return 'halaman terlarang';                                
        }
        return $next($request);
    }
    
    // public function handle($request, Closure $next, $role)
    // {
    //     if (!$request->user()->hasRole($role)) {
    //         return 'Halaman terlarang';
    //     }

    //     return $next($request);
    // }
}
