<?php

namespace App\Http\Controllers;

use DB;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function register(Request $request)
    {
        $hasher = app()->make('hash');
        $roles  = DB::table('roles')->select('id')->where('roleName', '=', 'kasir')->first()->id;
        $roles_id = $roles;
        $username = $request->input('username');
        $name = $request->input('nama');
        $email = $request->input('email');
        $password = $hasher->make($request->input('password'));
        $register = User::create([
            'roles_id'  => $roles_id,
            'username'  => $username,
            'name'      => $name,
            'email'     => $email,
            'password'  => $password
        ]);
        
        if ($register) {
            $res['success'] = true;
            $res['message'] = 'Success register!';
            return response($res);
        }else{
            $res['success'] = false;
            $res['message'] = 'Failed to register!';
            return response($res);
        }
    }

}
