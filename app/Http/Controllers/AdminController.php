<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\Menu;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function allMenu()
    {
        $all_menu = Menu::where('menus_status', '=', 'ready')->get();
          if (count($all_menu) !== 0) {
              $res['success'] = true;
              $res['result'] = $all_menu;
              return response($res);
          }else{
              $res['success'] = true;
              $res['result'] = 'Tidak ada menu yang tersedia';
              return response($res);
          }
    }

    public function show($id)
    {
        $id = Order::find($id);
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga','menus.menus_id' , 'orders.*')
            ->where('orders.orders_no_pesanan', '=', $id->orders_no_pesanan)
            ->get();

        if ($orders !== null) {
          $res['success'] = true;
          $res['result'] = $orders;
          return response($res);
        }else{
          $res['success'] = false;
          $res['result'] = 'Pesanan tidak ada!';
          return response($res);
        }
    }

    public function addPost(Request $request)
    {
      $this->validate($request, [
                      'jenis' => 'required',
                      'desc'  => 'required',
                      'harga' => 'required',
                      'status' => 'required'             
      ]);

      $menu = new Menu;
      $menu->fill([
        'menus_jenis' => $request->input('jenis'),
        'menus_desc' => $request->input('desc'),
        'menus_harga' => $request->input('harga'),
        'menus_status' => $request->input('status')
      ]);

      if($menu->save()){
        $res['success'] = true;
        $res['result'] = 'Berhasil menambahkan menu';
        return response($res);
      }
    }

    public function update(Request $request, $id)
    {      
      $menu = Menu::where('menus_id',$id)->first();
      $menu->menus_jenis = $request->input('jenis');
      $menu->menus_desc = $request->input('desc');
      $menu->menus_harga = $request->input('harga');
      $menu->menus_status = $request->input('status');

        if ($menu->save()) {
            $res['success'] = true;
            $res['result'] = 'Success update '.$request->input('menus_desc');
            return response($res);
          
        }else{
          $res['success'] = false;
          $res['result'] = 'Isi kolom yang kosong !';
          return response($res);
        }  

    }

    public function destroy($id)
    {
      $delete = Menu::find($id);
      if ($delete->delete($id)) {
          $res['success'] = true;
          $res['result'] = 'Berhasil hapus menu!';
          return response($res);
      }
    } 
}
