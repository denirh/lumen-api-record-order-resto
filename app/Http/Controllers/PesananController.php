<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Models\Menu;
use App\Models\Order;
use App\Models\Transaction;

class PesananController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
                ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
                ->select('menus.menus_desc', 'menus.menus_harga', 'orders.*')
                ->groupBy('orders.orders_customer')
                ->get();
          if (count($orders) !== 0) {
              $res['success'] = true;
              $res['result'] = $orders;
              return response($res);
          }else{
              $res['success'] = true;
              $res['result'] = 'Tidak ada menu yang tersedia';
              return response($res);
          }
    }

    public function show($id)
    {
        $id = Order::find($id);
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga','menus.menus_id' , 'orders.*')
            ->where('orders.orders_no_pesanan', '=', $id->orders_no_pesanan)
            ->get();

          if (count($orders) !== 0) {
              $res['success'] = true;
              $res['result'] = $orders;
              return response($res);
          }else{
              $res['success'] = true;
              $res['result'] = 'Tidak ada pesanan';
              return response($res);
          }
    }

    public function update(Request $request)
    {
        $menu= Menu::all();
        $i=0;
        $order = '';


            foreach($menu as $value){
              $menus=$value->menus_id;
                if($request->$menus!=''){
                    $found=false;
                    $findOrder=Order::where('orders_no_meja','=',$request->no_meja)->where('orders_no_pesanan','=',$request->no_pesanan)->get();
                    foreach ($findOrder as $value1) {
                        if ($value1->orders_menu_id==$menus) {
                            $found=true;
                        }
                       
                    }
                    
                    if ($found==true) {
                    $findId=Order::where('orders_no_meja','=',$request->no_meja)->where('orders_no_pesanan','=',$request->no_pesanan)->where('orders_menu_id','=',$menus)->first();
                    $order.$i = Order::find($findId->orders_id);
                    $order.$i->orders_customer = $request->customer;
                    $order.$i->orders_no_meja = $request->no_meja;
                    $order.$i->orders_menu_id = $value->menus_id;
                    $order.$i->orders_jumlah_pesan = $request->$menus;
                    $order.$i->save();
                    }else{
                        $order.$i=Order::create([        
                        'orders_customer'       => $request->customer,
                        'orders_no_pesanan'     => $request->no_pesanan,
                        'orders_no_meja'        => $request->no_meja,           
                        'orders_menu_id'        => $value->menus_id,            
                        'orders_jumlah_pesan'   => $request->$menus
                    ]);
                    }
                    $i++;
                   
                }
            }

        return response('Berhasil Merubah Pesanan');   
    }

    public function destroy($id)
    {
        $del='';
        $order = DB::table('orders')->where('orders_id','=',$id)->first();
        $orders = DB::table('orders')->where('orders_no_pesanan','=',$order->orders_no_pesanan)->get();
        foreach ($orders as $key) {
            $del.$i=DB::table('orders')->where('orders_id','=',$key->orders_id)->delete();
        }

        return response('Berhasil Menghapus Pesanan');
    }

    public function transaction(Request $request)
    {        
        $bayar='';
            $bayar = Transaction::create([
                    'transactions_no_pesanan' =>$request->no_pesan,
                    'transactions_harga' => $request->bayar,
                    'transactions_status' => 'finish'
            ]);
        $bayar->save();

        return response('Berhasil Melakukan Pembayaran');     
        
    }


}
