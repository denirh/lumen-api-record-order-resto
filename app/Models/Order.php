<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Model item ads
 */
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [ 'orders_id', 'orders_customer', 'orders_no_pesanan', 'orders_no_meja', 'orders_menu_id', 'orders_jumlah_pesan', ];
    protected $primaryKey = 'orders_id';
}