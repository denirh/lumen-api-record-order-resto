<?php

namespace Models\App;

use Illuminate\Database\Eloquent\Model;
/**
 * Model item ads
 */
class Role extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $fillable = ['roleName'];

    public function user()
    {
      return $this->belongsTo('App\Models\User', 'roles_id', 'id');
    }
}