<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Model item ads
 */
class Menu extends Model
{
    protected $table = 'menus';
    protected $fillable = [ 'menus_id', 'menus_jenis', 'menus_desc', 'menus_harga', 'menus_status' ];
    protected $primaryKey = 'menus_id';
}