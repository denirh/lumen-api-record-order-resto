<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'roles_id', 'username', 'name', 'email', 'password', 'remember_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->hasOne('App\Models\Role', 'roles_id', 'id');
    }

    public static function hasRole($role)
    {
        // if($this->role->roleName == $roleName){
        //     return true;
        // }
        //     return false;
        $id = \Auth::user()->id;
        return User::find($id)->role()->first()->roleName == $roleName ? true : false;
    }
}
