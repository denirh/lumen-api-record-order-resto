<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('register', 'RegisterController@register');
    $router->post('login', 'LoginController@login');
});

$router->get('/user/{id}', ['middleware' => 'auth', 'uses' =>  'LoginController@get_user']);

$router->group(['prefix' => 'admin'], function () use ($router) {
    $router->get('/menu', ['middleware' => 'role:admin', 'uses' =>  'AdminController@allMenu']);
    $router->get('/pesanan/{id}', ['middleware' => 'role:admin', 'uses' =>  'AdminController@show']);
    $router->post('/addMenu', ['middleware' => 'role:admin', 'uses' =>  'AdminController@addPost']);
    $router->post('/updateMenu/{id}', ['middleware' => 'role:admin', 'uses' =>  'AdminController@update']);
    $router->post('/deleteMenu/{id}', ['middleware' => 'role:admin', 'uses' =>  'AdminController@destroy']);
});

$router->group(['prefix' => 'cashier'], function () use ($router) {
    $router->get('/menu', ['middleware' => 'role:kasir', 'uses' =>  'AdminController@allMenu']);
    $router->get('/pesanan', ['middleware' => 'role:kasir', 'uses' =>  'PesananController@index']);
    $router->get('/pesanan/{id}', ['middleware' => 'role:kasir', 'uses' =>  'PesananController@show']);
    $router->post('/update/', ['middleware' => 'role:kasir', 'uses' =>  'PesananController@update']);
    $router->post('/delete/{id}', ['middleware' => 'role:kasir', 'uses' =>  'PesananController@destroy']);
    $router->post('/bayar/{id}', ['middleware' => 'role:kasir', 'uses' =>  'PesananController@destroy']);
    $router->post('/bayar/{id}', ['middleware' => 'role:kasir', 'uses' =>  'PesananController@transaction']);
});